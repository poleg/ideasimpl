﻿using BrightstarDB.EntityFramework;

namespace ideasImpl.DataLayer
{
    [Entity]
    public interface IPlotSettings
    {
        /// <summary>
        /// Get the persistent identifier for this entity
        /// </summary>
        string Id { get; }

        bool ShowRwaveReference { get; set; }
        bool ShowEditedEvents { get; set; }
        string XAxis { get; set; }
    }
}
