﻿using BrightstarDB.EntityFramework;

namespace ideasImpl.DataLayer
{
    [Entity]
    public interface IShowMarks
    {
        /// <summary>
        /// Get the persistent identifier for this entity
        /// </summary>
        string Id { get; }

        bool POnset { get; set; }
        bool QOnset { get; set; }
        bool SOnset { get; set; }
        bool TOnset { get; set; }
        bool RwavePeak { get; set; }
    }
}
