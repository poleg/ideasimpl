﻿using BrightstarDB.EntityFramework;

namespace ideasImpl.DataLayer
{
    [Entity]
    public interface ISearchCriterias
    {
        /// <summary>
        /// Get the persistent identifier for this entity
        /// </summary>
        string Id { get; }

        bool Tachy { get; set; }
        bool VpcEscape { get; set; }
        bool Couplet { get; set; }
        bool Triplet { get; set; }
        bool Bigeminy { get; set; }
        bool AvBlock { get; set; }
        bool Brady { get; set; }
        bool Pause { get; set; }
        bool Apc { get; set; }
        bool CustomTypes { get; set; }
        bool UninterpretableZone { get; set; }
        bool AtrialFibrillation { get; set; }
    }
}
