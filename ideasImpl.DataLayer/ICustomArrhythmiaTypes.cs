﻿using BrightstarDB.EntityFramework;

namespace ideasImpl.DataLayer
{
    [Entity]
    public interface ICustomArrhythmiaTypes
    {
        /// <summary>
        /// Get the persistent identifier for this entity
        /// </summary>
        string Id { get; }

        string Title { get; set; }
        bool IsSelected { get; set; }
    }
}
