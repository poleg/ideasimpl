﻿using BrightstarDB.EntityFramework;

namespace ideasImpl.DataLayer
{
    [Entity]
    public interface IColorSettings
    {
        /// <summary>
        /// Get the persistent identifier for this entity
        /// </summary>
        string Id { get; }

        string UninterpretableColor { get; set; }
        string ArrhythmiaBarColor { get; set; }
    }
}
