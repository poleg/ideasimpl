using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using ideasImpl.Model;
using ideasImpl.UndoRedo;

namespace ideasImpl.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private ComboDataItemCollection _customArrhythmiaTypes;
        private string _selectedCustomArrhythmiaType;
        
        private readonly IDataService _dataService;

        public MainViewModel(IDataService dataService)
        {
            _dataService = dataService;
            _dataService.Load();

            InitializeArrhythmiaTypesView();

            UndoCommand = new RelayCommand(Caretaker.Current.Undo, Caretaker.Current.IsUndoPossible);
            RedoCommand = new RelayCommand(Caretaker.Current.Redo, Caretaker.Current.IsRedoPossible);
            SaveCommand = new RelayCommand(Save, Caretaker.Current.IsSavePossible);

            AddArrhythmiaTypeCommand = new RelayCommand(AddArrhythmiaType, CanAddArrhythmiaType);
            DeleteArrhythmiaTypeCommand = new RelayCommand(DeleteArrhythmiaType, CanDeleteArrhythmiaType);
        }

        #region Commands

        public ICommand UndoCommand { get; private set; }

        public ICommand RedoCommand { get; private set; }

        public ICommand SaveCommand { get; private set; }

        public ICommand AddArrhythmiaTypeCommand { get; private set; }

        public ICommand DeleteArrhythmiaTypeCommand { get; private set; }

        #endregion

        public IEnumerable<DataItem> ShowMarksModel
        {
            get { return _dataService.ShowMarks; }
        }

        public IEnumerable<DataItem> SearchCriterias
        {
            get { return _dataService.SearchCriterias; }
        }

        public ICollectionView CustomArrhythmiaTypesView { get; private set; }

        public string SelectedCustomArrhythmiaType
        {
            get { return _selectedCustomArrhythmiaType; }
            set
            {
                _selectedCustomArrhythmiaType = value;
                _customArrhythmiaTypes.Select(
                    _customArrhythmiaTypes.Cast<ComboDataItem>().FirstOrDefault(x => x.Title == value));
            }
        }

        public IEnumerable<DataItem> ColorSettings
        {
            get { return _dataService.ColorSettings; }
        }

        public IEnumerable<DataItem> PlotSettings
        {
            get { return _dataService.PlotSettings.Take(2); }
        }

        public TextDataItem PlotSettingsXAxis
        {
            get { return _dataService.PlotSettings.ToArray()[2] as TextDataItem; }
        }

        private void Save()
        {
            _dataService.Save();
            Caretaker.Current.Reset();
        }

        private void AddArrhythmiaType()
        {
            _customArrhythmiaTypes.Add(new ComboDataItem(SelectedCustomArrhythmiaType)
            {
                IsSelected = true
            });
        }

        private void DeleteArrhythmiaType()
        {
            _customArrhythmiaTypes.Remove(CustomArrhythmiaTypesView.CurrentItem as ComboDataItem);
        }

        private bool CanAddArrhythmiaType()
        {
            return CustomArrhythmiaTypesView.CurrentItem == null &&
                   !string.IsNullOrEmpty(SelectedCustomArrhythmiaType) &&
                   _customArrhythmiaTypes.Count(x => x.Title == SelectedCustomArrhythmiaType) == 0;
        }

        private bool CanDeleteArrhythmiaType()
        {
            return CustomArrhythmiaTypesView.CurrentItem != null;
        }

        private void InitializeArrhythmiaTypesView()
        {
            _customArrhythmiaTypes = _dataService.CustomArrhythmiaTypes;
            CustomArrhythmiaTypesView = CollectionViewSource.GetDefaultView(_customArrhythmiaTypes.GetItems());
            _customArrhythmiaTypes.CollectionReset += OnCustomArrhythmiaTypesReset;
            _selectedCustomArrhythmiaType = _customArrhythmiaTypes.Cast<ComboDataItem>().Where(x => x.IsSelected)
                .Select(x => x.Title).FirstOrDefault();
        }
        private void OnCustomArrhythmiaTypesReset(object sender, EventArgs e)
        {
            _selectedCustomArrhythmiaType = _customArrhythmiaTypes.Cast<ComboDataItem>().Where(x => x.IsSelected)
                .Select(x => x.Title).FirstOrDefault();

            RaisePropertyChanged("CustomArrhythmiaTypesView");
            RaisePropertyChanged("SelectedCustomArrhythmiaType");
        }

    }
}