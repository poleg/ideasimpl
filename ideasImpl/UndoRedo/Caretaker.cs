﻿using System.Collections.Generic;
using ideasImpl.Model;
using Microsoft.Practices.ServiceLocation;

namespace ideasImpl.UndoRedo
{
    public class Caretaker
    {
        private readonly IOriginator _originator;

        private readonly Stack<Memento> _undoStack = new Stack<Memento>();
        private readonly Stack<Memento> _redoStack = new Stack<Memento>();

        private Caretaker(IOriginator originator)
        {
            _originator = originator;
        }

        public bool IsUndoPossible()
        {
            return _undoStack.Count > 0;
        }

        public bool IsRedoPossible()
        {
            return _redoStack.Count > 0;
        }

        public bool IsSavePossible()
        {
            return _undoStack.Count > 0;
        }

        public void Undo()
        {
            _redoStack.Push(_originator.CreateMemento());
            var memento = _undoStack.Pop();
            _originator.SetMemento(memento);
        }

        public void Redo()
        {
            _undoStack.Push(_originator.CreateMemento());
            var memento = _redoStack.Pop();
            _originator.SetMemento(memento);
        }

        public void SaveState()
        {
            _undoStack.Push(_originator.CreateMemento());
            _redoStack.Clear();
        }

        public void Reset()
        {
            _undoStack.Clear();
            _redoStack.Clear();
        }

        #region Singleton

        private static Caretaker _instance;

        public static Caretaker Current
        {
            get { return _instance ?? 
                (_instance = new Caretaker(ServiceLocator.Current.GetInstance<IDataService>())); }
        }

        #endregion
    }
}
