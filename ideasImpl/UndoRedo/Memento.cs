﻿using System.Collections.Generic;
using ideasImpl.Model;

namespace ideasImpl.UndoRedo
{
    public class Memento
    {
        private readonly IDictionary<string, DataItemCollection> _state;

        public Memento(IDictionary<string, DataItemCollection> state)
        {
            _state = state;
        }

        public IDictionary<string, DataItemCollection> GetState()
        {
            return _state;
        }
    }
}
