﻿namespace ideasImpl.UndoRedo
{
    public interface IOriginator
    {
        Memento CreateMemento();
        void SetMemento(Memento memento);
    }
}
