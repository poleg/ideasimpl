﻿using System;
using System.Collections.Generic;
using System.Linq;
using ideasImpl.UndoRedo;

namespace ideasImpl.Model
{
    public class ComboDataItemCollection : DataItemCollection
    {
        public ComboDataItemCollection(ExtendedObservableCollection<ComboDataItem> items) 
            : base(items)
        {
        }

        public event EventHandler CollectionReset;

        public void Add(ComboDataItem newItem)
        {
            Caretaker.Current.SaveState();

            var dataItems = Items as ExtendedObservableCollection<ComboDataItem>;

            if (dataItems != null) 
                dataItems.Add(newItem);
        }

        public void Remove(ComboDataItem item)
        {
            Caretaker.Current.SaveState();
            
            var dataItems = Items as ExtendedObservableCollection<ComboDataItem>;

            if (dataItems != null)
                dataItems.Remove(item);
        }

        public void Select(ComboDataItem item)
        {
            if(item == null)
                return;

            Caretaker.Current.SaveState();
            
            var oldSelected = Items.Cast<ComboDataItem>().FirstOrDefault(x => x.IsSelected);

            if (oldSelected != null)
                oldSelected.IsSelected = false;

            item.IsSelected = true;
        }

        public ExtendedObservableCollection<ComboDataItem> GetItems()
        {
            return Items as ExtendedObservableCollection<ComboDataItem>;
        }

        public override DataItemCollection Clone()
        {
            var list = CloneList().Cast<ComboDataItem>();

            return new ComboDataItemCollection(new ExtendedObservableCollection<ComboDataItem>(list));
        }

        public override void Update(IEnumerable<DataItem> items)
        {
            var observableCollection = Items as ExtendedObservableCollection<ComboDataItem>;

            if (observableCollection != null)
            {
                observableCollection.Replace(items.Cast<ComboDataItem>());
                OnCollectionReset();
            }
        }

        protected virtual void OnCollectionReset()
        {
            var handler = CollectionReset;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }
    }
}
