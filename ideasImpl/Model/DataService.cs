using System.Collections.Generic;
using ideasImpl.UndoRedo;

namespace ideasImpl.Model
{
    public class DataService : IDataService
    {
        private readonly IDataAccess _dataAccess;

        public DataService(IDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public DataItemCollection ShowMarks { get; private set; }

        public DataItemCollection SearchCriterias { get; private set; }

        public ComboDataItemCollection CustomArrhythmiaTypes { get; private set; }

        public DataItemCollection ColorSettings { get; private set; }

        public DataItemCollection PlotSettings { get; private set; }

        public void Load()
        {
            ShowMarks = _dataAccess.LoadShowMarks();
            SearchCriterias = _dataAccess.LoadSearchCriterias();
            CustomArrhythmiaTypes = _dataAccess.LoadCustomArrhythmiaTypes();
            ColorSettings = _dataAccess.LoadColorSettings();
            PlotSettings = _dataAccess.LoadPlotSettings();
        }

        public void Save()
        {
            _dataAccess.Save(ShowMarks,
                SearchCriterias,
                CustomArrhythmiaTypes,
                ColorSettings,
                PlotSettings);
        }

        #region IOriginator

        public Memento CreateMemento()
        {
            var state = new Dictionary<string, DataItemCollection>
            {
                {"ShowMarks", ShowMarks.Clone()},
                {"SearchCriterias", SearchCriterias.Clone()},
                {"CustomArrhythmiaTypes", CustomArrhythmiaTypes.Clone()},
                {"ColorSettings", ColorSettings.Clone()},
                {"PlotSettings", PlotSettings.Clone()}
            };

            return new Memento(state);
        }

        public void SetMemento(Memento memento)
        {
            var state = memento.GetState();

            ShowMarks.Update(state["ShowMarks"]);
            SearchCriterias.Update(state["SearchCriterias"]);
            CustomArrhythmiaTypes.Update(state["CustomArrhythmiaTypes"]);
            ColorSettings.Update(state["ColorSettings"]);
            PlotSettings.Update(state["PlotSettings"]);
        }

        #endregion
    }
}