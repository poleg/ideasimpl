﻿using System;
using System.IO;

namespace ideasImpl.Model
{
    public class DataAccessConfiguration
    {
        /// <summary>
        /// OPTIONAL: If you would like the samples to store their data in a different folder, replace
        /// the value of this property with the path to use (the directory will be created if it does not exist)
        /// </summary>
        public static readonly string StoresDirectory = 
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "ideasImplData");

        public static void Register()
        {
            // Ensure that the directory we want to use for storing samples data exists.
            // If it does not, create it.
            var dir = new DirectoryInfo(StoresDirectory);
            if (!dir.Exists)
            {
                dir.Create();
            }
        }
    }
}
