﻿namespace ideasImpl.Model.Design
{
    class DesignDataAccess : IDataAccess
    {
        public DataItemCollection LoadShowMarks()
        {
            return new DataItemCollection(new DataItem[]
            {
                new CheckDataItem("P-Onset", true),
                new CheckDataItem("Q-Onset", false),
                new CheckDataItem("S-Onset", true),
                new CheckDataItem("T-Onset", true),
                new CheckDataItem("R-wave peak", false)
            });
        }

        public DataItemCollection LoadSearchCriterias()
        {
            return new DataItemCollection(new DataItem[]
            {
                new CheckDataItem("Tachy", true),
                new CheckDataItem("VPC\\Escape", false),
                new CheckDataItem("Couplet", true),
                new CheckDataItem("Triplet", false),
                new CheckDataItem("Bigeminy", true),
                new CheckDataItem("AV Block", false),
                new CheckDataItem("Brady", false),
                new CheckDataItem("Pause", false),
                new CheckDataItem("APC", true),
                new CheckDataItem("Custom Types", true),
                new CheckDataItem("Uninterpretable Zone", false),
                new CheckDataItem("Atrial Fibrillation", true)
            });
        }

        public ComboDataItemCollection LoadCustomArrhythmiaTypes()
        {
            return new ComboDataItemCollection(new ExtendedObservableCollection<ComboDataItem>(new[]
            {
                new ComboDataItem("First") {IsSelected = false},
                new ComboDataItem("Second") {IsSelected = true},
                new ComboDataItem("Third") {IsSelected = false}
            }));
        }

        public DataItemCollection LoadColorSettings()
        {
            return new DataItemCollection(new DataItem[]
            {
                new TextDataItem("Uninterpretable", "#FF7A0D0D"),
                new TextDataItem("Arrhythmia bar", "#FF0DB8A0")
            });
        }

        public DataItemCollection LoadPlotSettings()
        {
            return new DataItemCollection(new DataItem[]
            {
                new CheckDataItem("Show R-wave reference #", false),
                new CheckDataItem("Show edited events", true),
                new TextDataItem("X-Axis (MN:SC)", "Normal")
            });
        }

        public void Save(DataItemCollection showMarks, DataItemCollection searchCriterias, DataItemCollection customArrhythmiaTypes,
            DataItemCollection colorSettings, DataItemCollection plotSettings)
        {
            
        }
    }
}
