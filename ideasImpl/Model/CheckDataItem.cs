﻿using ideasImpl.UndoRedo;

namespace ideasImpl.Model
{
    public class CheckDataItem : DataItem
    {
        private bool _isChecked;

        public CheckDataItem(string title, bool isChecked) 
            : base(title)
        {
            _isChecked = isChecked;
        }

        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                Caretaker.Current.SaveState();
                _isChecked = value;
                RaisePropertyChanged();
            }
        }

        public override void Update(DataItem currentItem)
        {
            var newItem = currentItem as CheckDataItem;

            if (newItem != null && 
                newItem.IsChecked != _isChecked)
            {
                _isChecked = newItem.IsChecked;
                RaisePropertyChanged("IsChecked");
            }
        }

        public override DataItem Clone()
        {
            return new CheckDataItem(Title, _isChecked);
        }
    }
}
