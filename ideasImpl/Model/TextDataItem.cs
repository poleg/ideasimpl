﻿using ideasImpl.UndoRedo;

namespace ideasImpl.Model
{
    public class TextDataItem : DataItem
    {
        private string _text;

        public TextDataItem(string title, string text) : 
            base(title)
        {
            _text = text;
        }

        public string Text
        {
            get { return _text; }
            set
            {
                Caretaker.Current.SaveState();
                _text = value;
                RaisePropertyChanged();
            }
        }

        public override void Update(DataItem currentItem)
        {
            var newItem = currentItem as TextDataItem;

            if (newItem != null &&
                System.String.CompareOrdinal(newItem.Text, _text) != 0)
            {
                _text = newItem.Text;
                RaisePropertyChanged("Text");
            }
        }

        public override DataItem Clone()
        {
            return new TextDataItem(Title, _text);
        }
    }
}
