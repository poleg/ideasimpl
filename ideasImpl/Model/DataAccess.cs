﻿using System;
using System.Linq;
using ideasImpl.DataLayer;

namespace ideasImpl.Model
{
    public class DataAccess : IDataAccess
    {
        private readonly string _connectionString;

        public DataAccess()
        {
            DataAccessConfiguration.Register();

            _connectionString = string.Format(
                @"Type=embedded;storesDirectory={0};StoreName=ArrhythmiaSettings;",
                DataAccessConfiguration.StoresDirectory);
        }

        #region load

        public DataItemCollection LoadShowMarks()
        {
            using (var ctx = new DataLayer.SettingsContext(_connectionString))
            {
                var showMarks = ctx.ShowMarkss.FirstOrDefault();

                return new DataItemCollection(new DataItem[]
                {
                    new CheckDataItem("P-Onset", (showMarks != null) && showMarks.POnset),
                    new CheckDataItem("Q-Onset", (showMarks != null) && showMarks.QOnset),
                    new CheckDataItem("S-Onset", (showMarks != null) && showMarks.SOnset),
                    new CheckDataItem("T-Onset", (showMarks != null) && showMarks.TOnset),
                    new CheckDataItem("R-wave peak", (showMarks != null) && showMarks.RwavePeak)
                });
            }
        }

        public DataItemCollection LoadSearchCriterias()
        {
            using (var ctx = new DataLayer.SettingsContext(_connectionString))
            {
                var searchCriterias = ctx.SearchCriteriass.FirstOrDefault();

                return new DataItemCollection(new DataItem[]
                {
                    new CheckDataItem("Tachy", searchCriterias != null && searchCriterias.Tachy),
                    new CheckDataItem("VPC\\Escape", searchCriterias != null && searchCriterias.VpcEscape),
                    new CheckDataItem("Couplet", searchCriterias != null && searchCriterias.Couplet),
                    new CheckDataItem("Triplet", searchCriterias != null && searchCriterias.Triplet),
                    new CheckDataItem("Bigeminy", searchCriterias != null && searchCriterias.Bigeminy),
                    new CheckDataItem("AV Block", searchCriterias != null && searchCriterias.AvBlock),
                    new CheckDataItem("Brady", searchCriterias != null && searchCriterias.Brady),
                    new CheckDataItem("Pause", searchCriterias != null && searchCriterias.Pause),
                    new CheckDataItem("APC", searchCriterias != null && searchCriterias.Apc),
                    new CheckDataItem("Custom Types", searchCriterias != null && searchCriterias.CustomTypes),
                    new CheckDataItem("Uninterpretable Zone", searchCriterias != null && searchCriterias.UninterpretableZone),
                    new CheckDataItem("Atrial Fibrillation", searchCriterias != null && searchCriterias.AtrialFibrillation)
                });
            }
        }

        public ComboDataItemCollection LoadCustomArrhythmiaTypes()
        {
            using (var ctx = new DataLayer.SettingsContext(_connectionString))
            {
                var list = ctx.CustomArrhythmiaTypess.ToList();
                    
                var comboList = list.Select(x =>
                    new ComboDataItem(x.Title) {IsSelected = x.IsSelected});

                return
                    new ComboDataItemCollection(
                        new ExtendedObservableCollection<ComboDataItem>(comboList));
            }
        }

        public DataItemCollection LoadColorSettings()
        {
            using (var ctx = new DataLayer.SettingsContext(_connectionString))
            {
                var colorSettings = ctx.ColorSettingss.FirstOrDefault();


                return new DataItemCollection(new DataItem[]
                {
                    new TextDataItem("Uninterpretable",
                        (colorSettings != null) ? colorSettings.UninterpretableColor : "#FF7A0D0D"),
                    new TextDataItem("Arrhythmia bar",
                        (colorSettings != null) ? colorSettings.ArrhythmiaBarColor : "#FF0DB8A0")
                });
            }
        }

        public DataItemCollection LoadPlotSettings()
        {
            using (var ctx = new DataLayer.SettingsContext(_connectionString))
            {
                var plotSettings = ctx.PlotSettingss.FirstOrDefault();

                return new DataItemCollection(new DataItem[]
                {
                    new CheckDataItem("Show R-wave reference #", plotSettings != null && plotSettings.ShowRwaveReference),
                    new CheckDataItem("Show edited events", plotSettings != null && plotSettings.ShowEditedEvents),
                    new TextDataItem("X-Axis (MN:SC)", (plotSettings != null) ? plotSettings.XAxis : string.Empty)
                });
            }
        }

        #endregion

        #region save

        public void Save(DataItemCollection showMarks, DataItemCollection searchCriterias,
            DataItemCollection customArrhythmiaTypes,
            DataItemCollection colorSettings, DataItemCollection plotSettings)
        {
            using (var ctx = new DataLayer.SettingsContext(_connectionString))
            {
                var showMarksEntity = ctx.ShowMarkss.FirstOrDefault() ?? ctx.ShowMarkss.Create();
                FillShowMarks(showMarks, showMarksEntity);

                var searchCriteriasEntity = ctx.SearchCriteriass.FirstOrDefault() ?? ctx.SearchCriteriass.Create();
                FillSearchCriterias(searchCriterias, searchCriteriasEntity);

                foreach (var customArrhythmiaType in customArrhythmiaTypes.Cast<ComboDataItem>())
                {
                    var type = ctx.CustomArrhythmiaTypess.FirstOrDefault(x => x.Title == customArrhythmiaType.Title)
                               ?? ctx.CustomArrhythmiaTypess.Create();

                    type.Title = customArrhythmiaType.Title;
                    type.IsSelected = customArrhythmiaType.IsSelected;
                }

                var colorSettingsEntity = ctx.ColorSettingss.FirstOrDefault() ?? ctx.ColorSettingss.Create();
                FillColorSettings(colorSettings, colorSettingsEntity);

                var plotSettingsEntity = ctx.PlotSettingss.FirstOrDefault() ?? ctx.PlotSettingss.Create();
                FillPlotSettings(plotSettings, plotSettingsEntity);

                ctx.SaveChanges();
            }
        }

        private static void FillPlotSettings(DataItemCollection plotSettings, IPlotSettings plotSettingsEntity)
        {
            plotSettingsEntity.ShowRwaveReference =
                plotSettings.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "Show R-wave reference #").IsChecked;
            plotSettingsEntity.ShowEditedEvents =
                plotSettings.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "Show edited events").IsChecked;
            
            var xAxis = plotSettings.FirstOrDefault(x => x.Title == "X-Axis (MN:SC)") as TextDataItem;

            plotSettingsEntity.XAxis = xAxis.Text;
        }

        private static void FillColorSettings(DataItemCollection colorSettings, IColorSettings colorSettingsEntity)
        {
            colorSettingsEntity.UninterpretableColor =
                colorSettings.Cast<TextDataItem>().FirstOrDefault(x => x.Title == "Uninterpretable").Text;
            colorSettingsEntity.ArrhythmiaBarColor =
                colorSettings.Cast<TextDataItem>().FirstOrDefault(x => x.Title == "Arrhythmia bar").Text;
        }

        private static void FillSearchCriterias(DataItemCollection searchCriterias, ISearchCriterias searchCriteriasEntity)
        {
            searchCriteriasEntity.Apc =
                searchCriterias.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "APC").IsChecked;
            searchCriteriasEntity.Tachy =
                searchCriterias.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "Tachy").IsChecked;
            searchCriteriasEntity.VpcEscape =
                searchCriterias.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "VPC\\Escape").IsChecked;
            searchCriteriasEntity.Couplet =
                searchCriterias.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "Couplet").IsChecked;
            searchCriteriasEntity.Triplet =
                searchCriterias.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "Triplet").IsChecked;
            searchCriteriasEntity.Bigeminy =
                searchCriterias.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "Bigeminy").IsChecked;
            searchCriteriasEntity.AvBlock =
                searchCriterias.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "AV Block").IsChecked;
            searchCriteriasEntity.Brady =
                searchCriterias.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "Brady").IsChecked;
            searchCriteriasEntity.Pause =
                searchCriterias.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "Pause").IsChecked;
            searchCriteriasEntity.CustomTypes =
                searchCriterias.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "Custom Types").IsChecked;
            searchCriteriasEntity.UninterpretableZone =
                searchCriterias.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "Uninterpretable Zone").IsChecked;
            searchCriteriasEntity.AtrialFibrillation =
                searchCriterias.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "Atrial Fibrillation").IsChecked;
        }

        private static void FillShowMarks(DataItemCollection showMarks, IShowMarks showMarksEntity)
        {
            showMarksEntity.POnset = showMarks.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "P-Onset").IsChecked;
            showMarksEntity.QOnset = showMarks.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "Q-Onset").IsChecked;
            showMarksEntity.TOnset = showMarks.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "T-Onset").IsChecked;
            showMarksEntity.SOnset = showMarks.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "S-Onset").IsChecked;
            showMarksEntity.RwavePeak = showMarks.Cast<CheckDataItem>().FirstOrDefault(x => x.Title == "R-wave peak").IsChecked;
        }
        
        #endregion
    }
}
