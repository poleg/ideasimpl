﻿using GalaSoft.MvvmLight;

namespace ideasImpl.Model
{
    public class DataItem : ObservableObject
    {
        public DataItem(string title)
        {
            Title = title;
        }

        public string Title { get; private set; }

        #region override

        public override string ToString()
        {
            return Title;
        }

        public override bool Equals(object other)
        {
            var item = other as DataItem;
            
            return item != null && string.Equals(Title, item.Title);
        }

        public override int GetHashCode()
        {
            return (Title != null ? Title.GetHashCode() : 0);
        }

        
        #endregion

        #region virtual

        public virtual void Update(DataItem currentItem)
        {

        }

        public virtual DataItem Clone()
        {
            return new DataItem(Title);
        }

        #endregion
    }
}
