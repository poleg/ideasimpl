using ideasImpl.UndoRedo;

namespace ideasImpl.Model
{
    public interface IDataService : IOriginator
    {
        DataItemCollection ShowMarks { get; }
        DataItemCollection SearchCriterias { get; }
        ComboDataItemCollection CustomArrhythmiaTypes { get; }
        DataItemCollection ColorSettings { get; }
        DataItemCollection PlotSettings { get; }

        void Load();
        void Save();
    }
}