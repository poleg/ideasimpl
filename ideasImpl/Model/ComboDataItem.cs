﻿namespace ideasImpl.Model
{
    public class ComboDataItem : DataItem
    {
        public ComboDataItem(string title) 
            : base(title)
        {
        }

        public bool IsSelected { get; set; }

        public override DataItem Clone()
        {
            return new ComboDataItem(Title)
            {
                IsSelected = IsSelected
            };
        }
    }
}
