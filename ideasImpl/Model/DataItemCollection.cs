﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ideasImpl.Model
{
    public class DataItemCollection : IEnumerable<DataItem>
    {
        protected readonly IEnumerable<DataItem> Items;

        public DataItemCollection(IEnumerable<DataItem> items)
        {
            Items = items;
        }

        public virtual void Update(IEnumerable<DataItem> items)
        {
            foreach (var currentItem in items)
            {
                var item = Items.FirstOrDefault(x => x.Equals(currentItem));
                if (item != null)
                {
                    item.Update(currentItem);
                }
            }
        }

        public virtual DataItemCollection Clone()
        {
            return new DataItemCollection(CloneList());
        }

        protected IEnumerable<DataItem> CloneList()
        {
            return Items.Select(currentItem => currentItem.Clone()).ToList();
        }

        #region IEnumerable

        public IEnumerator<DataItem> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
