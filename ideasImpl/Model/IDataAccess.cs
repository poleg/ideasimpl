﻿namespace ideasImpl.Model
{
    public interface IDataAccess
    {
        #region load

        DataItemCollection LoadShowMarks();
        DataItemCollection LoadSearchCriterias();
        ComboDataItemCollection LoadCustomArrhythmiaTypes();
        DataItemCollection LoadColorSettings();
        DataItemCollection LoadPlotSettings();

        #endregion

        #region save

        void Save(DataItemCollection showMarks,
            DataItemCollection searchCriterias,
            DataItemCollection customArrhythmiaTypes,
            DataItemCollection colorSettings,
            DataItemCollection plotSettings);

        #endregion
    }
}
