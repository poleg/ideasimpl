﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ideasImpl
{
    public class ExtendedObservableCollection<T> : ObservableCollection<T>
    {
        public ExtendedObservableCollection(IEnumerable<T> items)
            : base(items)
        {
        }

        public void Replace(IEnumerable<T> items)
        {
            Items.Clear();
            foreach (var item in items)
            {
                Items.Add(item);
            }
        }
    }
}
